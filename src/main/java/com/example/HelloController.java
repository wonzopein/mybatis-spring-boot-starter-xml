package com.example;

import com.example.mapper.CityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description
 *
 * @author wonzopein
 * @version 2015. 11. 29.
 */
@RestController
public class HelloController {


    @Autowired
    CityMapper cityMapper;

    @RequestMapping("/")
    String hello(){

        System.out.println(cityMapper.selectCityById(1));
        System.out.println(cityMapper.selectCitys());

        return "Hello!";
    }

}
